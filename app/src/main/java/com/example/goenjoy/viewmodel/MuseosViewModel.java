package com.example.goenjoy.viewmodel;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.goenjoy.model.Museo;
import com.example.goenjoy.room.Repository;

import java.util.List;

public class MuseosViewModel extends AndroidViewModel {
    private Repository mRepository;

    private LiveData<List<Museo>> mAllLugares;
    private LiveData<List<Museo>> mAllMuseo;
    private LiveData<List<Museo>> mAllParques;
    private LiveData<List<Museo>> mAllMuseoFav;
    private LiveData<List<Museo>> mAllMuseoDes;
    private LiveData<List<Museo>> mAllMuseoRuta;

    public MuseosViewModel(Application application) {
        super(application);
        mRepository = new Repository(application);
        mAllLugares = mRepository.getAllLugares();
        mAllMuseo = mRepository.getAllMuseos();
        mAllParques = mRepository.getAllParques();
        mAllMuseoFav = mRepository.getAllFav();
        mAllMuseoRuta = mRepository.getAllRuta();
        mAllMuseoDes = mRepository.getAllDes();
    }

    public LiveData<List<Museo>> getAllLugares() { return mAllLugares; }
    public LiveData<List<Museo>> getAllMuseos(){ return mAllMuseo;}
    public LiveData<List<Museo>> getAllMuseoFav() {return mAllMuseoFav;}
    public LiveData<List<Museo>> getAllParques(){ return mAllParques; }
    public LiveData<List<Museo>> getAllMuseoRuta() { return mAllMuseoRuta; }
    public LiveData<List<Museo>> getmAllMuseosDes() {return mAllMuseoDes;}
    public void insert(Museo museo) { mRepository.insert(museo); }
    public void update(Museo museo) { mRepository.update(museo); }
    public void delete(Museo museo) { mRepository.delete(museo); }
    public boolean existeMuseo(String title) { return mRepository.existe(title); }
    public void deleteAll() { mRepository.deleteAll(); }

    public void cargarDatos(){ mRepository.cargarDatos();}
}