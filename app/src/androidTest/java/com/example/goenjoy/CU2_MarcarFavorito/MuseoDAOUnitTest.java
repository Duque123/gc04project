package com.example.goenjoy.CU2_MarcarFavorito;

import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.example.goenjoy.model.Museo;
import com.example.goenjoy.room.MuseoDao;
import com.example.goenjoy.room.MuseoDatabase;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;

import android.content.Context;
import androidx.lifecycle.LiveData;
import androidx.room.Room;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.test.platform.app.InstrumentationRegistry;

import org.junit.rules.TestRule;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class MuseoDAOUnitTest {
    private MuseoDao museoDao;
    private MuseoDatabase mDatabase;

    @Rule
    public TestRule rule = new InstantTaskExecutorRule();


    @Before
    public void createMuseoDatabase() {
        //create a mock database, using our RoomDatabase implementation (AppDatabase.class)
        Context context = InstrumentationRegistry.getInstrumentation().getTargetContext();
        mDatabase = Room.inMemoryDatabaseBuilder(context, MuseoDatabase.class).allowMainThreadQueries().build();
        //take the dao from the created database
        museoDao = mDatabase.museoDao();
    }

    @After
    public void closeDb() throws IOException {
        //closing database
        mDatabase.close();
    }


    @Test
    public void writePerfilAndPutItOnFav() throws Exception {
        Museo museo = CrearMuseo(1);
        museoDao.insert(museo);

        museo.setFav(1);
        museoDao.update(museo);

        LiveData<List<Museo>> livemuseos = museoDao.getAll();
        List<Museo> museos = LiveDataTestUtils.getValue(livemuseos);
        assertEquals(1, museos.get(0).getId());
        assertEquals(1, museos.get(0).getFav());
    }

    public static Museo CrearMuseo(int id){
        Museo museo = new Museo(id, "title", "relation", "localidad", "postalCode", "streetAdress",
                40.414358466555235f, -3.6974741545860015f, "desc", "0", "schedule", 0, 0, 0,0);
        return museo;
    }


}
