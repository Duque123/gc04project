package com.example.goenjoy.CU03_CrearRuta;

import com.example.goenjoy.model.Museo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.lang.reflect.Field;

public class MuseoUnitTest {

    @Test
    public void setRutaTest() throws NoSuchFieldException, IllegalAccessException {
        int value = 1;
        com.example.goenjoy.model.Museo instance = new  com.example.goenjoy.model.Museo();
        instance.setRuta(value);
        final Field field = instance.getClass().getDeclaredField("ruta");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getRutaTest() throws NoSuchFieldException, IllegalAccessException {
        final  com.example.goenjoy.model.Museo instance = new  com.example.goenjoy.model.Museo();
        final Field field = instance.getClass().getDeclaredField("ruta");
        field.setAccessible(true);
        field.set(instance, 1);

        //when
        final int result = instance.getRuta();

        //then
        assertEquals(result,1);
    }
}
