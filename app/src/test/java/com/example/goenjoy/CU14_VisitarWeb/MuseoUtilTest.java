package com.example.goenjoy.CU14_VisitarWeb;

import org.junit.Test;

import java.lang.reflect.Field;

import static org.junit.Assert.assertEquals;

public class MuseoUtilTest {
    @Test
    public void setRelationTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "http://www.madrid.es/sites/v/index.jsp?vgnextchannel=bfa48ab43d6bb410VgnVCM100000171f5a0aRCRD&vgnextoid=fe43c4be8e688210VgnVCM1000000b205a0aRCRD";
        com.example.goenjoy.model.Museo instance = new  com.example.goenjoy.model.Museo();
        instance.setRelation(value);
        final Field field = instance.getClass().getDeclaredField("relation");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getRelationTest() throws NoSuchFieldException, IllegalAccessException {
        final  com.example.goenjoy.model.Museo instance = new  com.example.goenjoy.model.Museo();
        final Field field = instance.getClass().getDeclaredField("relation");
        field.setAccessible(true);
        field.set(instance, "http://www.madrid.es/sites/v/index.jsp?vgnextchannel=bfa48ab43d6bb410VgnVCM100000171f5a0aRCRD&vgnextoid=fe43c4be8e688210VgnVCM1000000b205a0aRCRD");

        //when
        final String result = instance.getRelation();

        //then
        assertEquals("field wasn't retrieved properly", result, "http://www.madrid.es/sites/v/index.jsp?vgnextchannel=bfa48ab43d6bb410VgnVCM100000171f5a0aRCRD&vgnextoid=fe43c4be8e688210VgnVCM1000000b205a0aRCRD");
    }
}
